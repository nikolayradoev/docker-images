# docker-images

Simple projet to store all Dockerfiles and images.

# CI

Gitlab CI tracks changes and Dockerfiles and rebuild images and push them in project container registry [https://gitlab.com/nikolayradoev/docker-images/container_registry](https://gitlab.com/nikolayradoev/docker-images/container_registry).
